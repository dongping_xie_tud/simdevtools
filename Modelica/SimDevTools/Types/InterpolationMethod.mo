within SimDevTools.Types;
type InterpolationMethod = enumeration(
    Nearest "Take the nearest value",
    Linear "Linear interpolation",
    Akima "Akima spline interpolation");
