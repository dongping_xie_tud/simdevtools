'''
Convert a result.h5 file form the Recorder block to SDF
'''

import os
import numpy as np
import h5py

if __name__ == '__main__':
    infile = 'result.h5'
    outfile = os.path.splitext(infile)[0] + '.sdf'
    
    # read the result file
    with h5py.File(infile, 'r') as r:
        time = r['time'].value
        names = r['signal_names'].value
        quantities = r['signal_quantities'].value
        units = r['signal_units'].value
        comments = r['signal_comments'].value
        values = r['signal_values'].value
        
    # write the SDF file    
    with h5py.File(outfile, 'w') as w:
        w['time'] = time
        ds_time = w['time']
        
        for i, name in enumerate(names):
            w[name] = values[:,i].flatten()
            ds = w[name]
            ds.attrs['QUANTITY'] = np.string_(quantities[i])
            ds.attrs['UNIT'] = np.string_(units[i])
            ds.attrs['COMMENT'] = np.string_(comments[i])
            ds.dims.create_scale(ds_time, 'Time')
            ds.dims[0].attach_scale(ds_time)
