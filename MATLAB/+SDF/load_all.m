% Copyright (C) 2014 Modelon GmbH. All rights reserved.
%
% This file is part of the Simulation Development Tools.
%
% This program and the accompanying materials are made
% available under the terms of the BSD 3-Clause License
% which accompanies this distribution, and is available at
% http://simdevtools.org/LICENSE.txt
%
% Contributors:
%   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation

function load_all(filename)
% load all datasets into the workspace

info = hdf5info(filename, 'ReadAttributes', false);

load_group(filename, info.GroupHierarchy)

end

function load_group(filename, gh)

% load the sub-groups
for group = gh.Groups
    load_group(filename, group)
end

for ds = gh.Datasets
    name = ds.Name(2:end); % strip the leading slash
    name = strrep(name, '/', '_'); % replace the slashes with underscores
    % TODO: replace other illegal characters
    value = hdf5read(filename, ds.Name);
    assignin('base', name, value);
end

end
