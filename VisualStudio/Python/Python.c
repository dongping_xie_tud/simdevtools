#include <stdlib.h>
#include "Python.h"


PYTHON_API double NDTable_add(double a, double b) {
	return a + b;
}

PYTHON_API ModelicaNDTable_h create_table(int ndims, const int *dims, const double *data, const double **scales) {
	int i, j;
	ModelicaNDTable_h table = NDTable_alloc_table();

	table->ndims = ndims;

	for(i = 0; i < ndims; i++) {
		table->dims[i] = dims[i];
		table->scales[i] = (double *)malloc(sizeof(double) * dims[i]);
		for(j = 0; j < dims[i]; j++) {
			table->scales[i][j] = scales[i][j];
		}
	}

	table->numel = NDTable_calculate_numel(table->ndims, table->dims);
	
	table->data = (double *)malloc(sizeof(double) * table->numel);
	for(i = 0; i < table->numel; i++) {
		table->data[i] = data[i];
	}

	return table;
}

PYTHON_API void close_table(ModelicaNDTable_h table) {
	int i;

	free(table->data);

	for(i = 0; i < MAX_NDIMS; i++) {
		free(table->scales[i]);
	}

	free(table);
}

PYTHON_API int evaluate(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value) {

	return NDTable_evaluate(table, nparams, params, interp_method, extrap_method, value);
}

PYTHON_API int evaluate_derivative(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[],
	const double delta_params[],
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value) {

		return NDTable_evaluate_derivative(table, nparams, params, delta_params, interp_method, extrap_method, value);
}
