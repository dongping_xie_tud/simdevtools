// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the PYTHON_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// PYTHON_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef PYTHON_EXPORTS
#define PYTHON_API __declspec(dllexport)
#else
#define PYTHON_API __declspec(dllimport)
#endif

#include "NDTable.h"

PYTHON_API double NDTable_add(double a, double b);

PYTHON_API ModelicaNDTable_h create_table(int ndims, const int *dims, const double **scales, const double *data);

PYTHON_API void close_table(ModelicaNDTable_h table);

PYTHON_API int evaluate(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[], 
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value);

PYTHON_API int evaluate_derivative(
	ModelicaNDTable_h table, 
	int nparams, 
	const double params[],
	const double delta_params[],
	ModelicaNDTable_InterpMethod_t interp_method,
	ModelicaNDTable_ExtrapMethod_t extrap_method, 
	double *value);