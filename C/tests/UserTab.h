/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef USERTAB_H
#define USERTAB_H

#include "ModelicaNDTable.h"

#define N_USERTABS 1

static double DS1_data[4]   = { 0.0, 1.0, 2.0, 3.0 };
static double DS1_scale0[2] = { 0.0, 1.0 };
static double DS2_scale1[2] = { 2.0, 3.0 };

static ModelicaNDTable_t userTabs[N_USERTABS] = {
	{ "usertab.sdf", // filename
	  "/DS1",		 // datasetname
	  2,			 // rank
	  {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1}, // dims
	  1,																 // numel 
	  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // offs
	  DS1_data,															 // data
	  {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales
	  "Q1",																 // data_quantity
	  "U1",																 // data_unit
	  {"SQ1","SQ2",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // scales_quantities
	  {"SU1","SU2",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // scales_units
	}
};

#endif