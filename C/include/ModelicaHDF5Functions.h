/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef MODELICAHDF5FUNCTIONS_H_
#define MODELICAHDF5FUNCTIONS_H_

#if defined(__cplusplus)
	#define MODELICA_HDF5FUNCTIONS_API extern "C"
#else
	#define MODELICA_HDF5FUNCTIONS_API extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_create_group(const char *filename, const char *group_name, const char *comment);

/*! Retrieves the dimensions of a dataset
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the dataset name
 * @param [out]	dims			a buffer for the dimensions
 *
 * @return		0 on success or -1 if the dimensions could not be retrieved
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_get_dataset_dims(const char *filename, const char *dataset_name, int dims[]);

/*! Reads the values of a double dataset
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the dataset name
 * @param [in]	quantity		the expected quantity
 * @param [in]	unit			the expected unit
 * @param [out]	buffer			a buffer for the values
 *
 * @return		0 on success, -1 otherwise
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_read_dataset_double(const char *filename, const char *dataset_name, const char *quantity, const char *unit, double *buffer);

/*! Reads the values of a integer dataset
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the dataset name
 * @param [in]	quantity		the expected quantity
 * @param [in]	unit			the expected unit
 * @param [out]	buffer			a buffer for the values
 *
 * @return		0 on success, -1 otherwise
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_read_dataset_int(const char *filename, const char *dataset_name, const char *quantity, const char *unit, int *buffer);

/*! Writes a double dataset with quantity, unit and comment
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the dataset name
 * @param [in]	ndims			the number of dimensions
 * @param [in]	dims			the dimensions
 * @param [in]	buffer			a buffer for the values
 * @param [in]	quantity		the quantity (optional)
 * @param [in]	unit			the unit (optional)
 * @param [in]	comment			the comment (optional)
 *
 * @return		0 on success, -1 otherwise
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_make_dataset_double(const char *filename, const char *dataset_name, int ndims, const int dims[], const double *buffer, const char *quantity, const char *unit, const char *comment);

/*! Writes an integer dataset with quantity, unit and comment
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the dataset name
 * @param [in]	ndims			the number of dimensions
 * @param [in]	dims			the dimensions
 * @param [in]	buffer			a buffer for the values
 * @param [in]	quantity		the quantity (optional)
 * @param [in]	unit			the unit (optional)
 * @param [in]	comment			the comment (optional)
 *
 * @return		0 on success, -1 otherwise
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_make_dataset_int(const char *filename, const char *dataset_name, int ndims, const int dims[], const int *buffer, const char *quantity, const char *unit, const char *comment);

/*! Sets a dataset as the scale for the dimension of another dataset
 * 
 * @param [in]	filename		the file name
 * @param [in]	dataset_name	the name of the dataset
 * @param [in]	scale_name		the name of the scale dataset
 * @param [in]	dim_name		the name of the dimension
 * @param [in]	dim				the index of the dimension
 *
 * @return		0 on success, -1 otherwise
 */
MODELICA_HDF5FUNCTIONS_API void ModelicaHDF5Functions_attach_scale(const char *filename, const char *dataset_name, const char *scale_name, const char *dim_name, int dim);

#ifdef __cplusplus
}
#endif

#endif /*MODELICAHDF5FUNCTIONS_H_*/