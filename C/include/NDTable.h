/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef NDTABLE_H_
#define NDTABLE_H_

#include "ModelicaNDTable.h"

#if defined(__cplusplus)
	#define MODELICA_NDTABLE_API extern "C"
#else
	#define MODELICA_NDTABLE_API extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*! Interpolation status codes */
typedef enum {
	NDTABLE_INTERPSTATUS_UNKNOWN_METHOD	= -4,
	NDTABLE_INTERPSTATUS_DATASETNOTFOUND	= -3,
	NDTABLE_INTERPSTATUS_WRONGNPARAMS		= -2,
	NDTABLE_INTERPSTATUS_OUTOFBOUNS		= -1,
    NDTABLE_INTERPSTATUS_OK				=  0
} NDTable_InterpolationStatus;


/*! Gets the last error message
 * 
 * @param [in]	length			the size of the message buffer
 * @param [in]	message			a buffer for the error message  
 * 
 * @return		the error string (same as message)
 */
MODELICA_NDTABLE_API char * NDTable_get_error_message(size_t length, char *message);

/*! Reads a dataset from an HDF5 file
 * 
 * @param [in]	filename	the name of the file to read (e.g. "data.h5", "resources/data.h5")
 * @param [in]	datasetname	the name of dataset to read (e.g. "/ds1", "/g1/ds1")
 * @param [in]	ndims		the number of dimensions of the table
 * @param [in]	reuse		whether a previously loaded dataset with the same file name and dataset name should be re-used
 * 
 * @return		the read status
 */
MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_read(const char *filename, const char *datasetname, const int ndims, const char *data_quantity, const char *data_unit, const char **scale_quantities, const char **scale_units, int share);

/*! Evalutes the value of the table at the given sample point using the specified inter- and extrapolation methods
 * 
 * @param [in]	table			the table handle
 * @param [in]	nparams			the number of dimensions
 * @param [in]	params			the sample point
 * @param [in]	interp_method	the interpolation method
 * @param [in]	extrap_method	the extrapolation method
 * @param [out]	value			the value at the sample point  
 * 
 * @return		0 if the value could be evaluated, -1 otherwise
 */
MODELICA_NDTABLE_API int NDTable_evaluate(ModelicaNDTable_h table, int nparams, const double params[], ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value);

/*! Evalutes the total differential of the table at the given sample point and deltas using the specified inter- and extrapolation methods
 * 
 * @param [in]	table			the table handle
 * @param [in]	nparams			the number of dimensions
 * @param [in]	params			the sample point
 * @param [in]	delta_params	the the deltas
 * @param [in]	interp_method	the interpolation method
 * @param [in]	extrap_method	the extrapolation method
 * @param [out]	value			the total differential at the sample point  
 * 
 * @return		0 if the value could be evaluated, -1 otherwise
 */
MODELICA_NDTABLE_API int NDTable_evaluate_derivative(ModelicaNDTable_h table, int nparams, const double params[], const double delta_params[], ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value);

/*! The maximum number of tables */
#define MAX_NTABLES 1000

/*! The maximum length of an error message */	
#define MAX_MESSAGE_LENGTH 256

#define COMMENT_ATTR_NAME "COMMENT"
#define UNIT_ATTR_NAME "UNIT"
#define QUANTITY_ATTR_NAME "QUANTITY"


/*! Finds a compiled-in table defined in UserTab.h */
MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_find_usertab(const char *filename, const char *datasetname);

/*! Sets the error message */
MODELICA_NDTABLE_API void NDTable_set_error_message(const char *msg, ...);

/*! Allocates a new table
 *
 *	@return a pointer to the new table
 */
MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_alloc_table();

/*! De-allocates a table
 *
 *	@param [in]	pointer to the table to de-allocate
 */
MODELICA_NDTABLE_API void NDTable_free_table(ModelicaNDTable_h table);

/*! Adds the table to the internal storage
 *
 *	@return the id of the table or -1 if the maximum number of tables has been reached
 */
MODELICA_NDTABLE_API int NDTable_add_table(const ModelicaNDTable_h table);

MODELICA_NDTABLE_API int NDTable_get_table_count();

MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_get_table(int table_id);

/*! Converts index to subscripts
 * 
 * @param [in]	index	the index to convert
 * @param [in]	table	the table for which to convert the index
 * @param [out]	subs	the subscripts
 */
MODELICA_NDTABLE_API void NDTable_ind2sub(const int index, const ModelicaNDTable_h table, int *subs);

/*! Converts subscripts to index
 * 
 *	@param [in]		subs	the subscripts to convert
 *	@param [in]		table	the table for which to convert the subscripts
 *	@param [out]	index	the index
 */
MODELICA_NDTABLE_API void NDTable_sub2ind(const int *subs, const ModelicaNDTable_h table, int *index);

MODELICA_NDTABLE_API double NDTable_get_value_subs(const ModelicaNDTable_h table, const int subs[]);

/*! Helper function that finds the indices for the interpolation
 *
 *  @param [in]		value		the value to search for 
 *  @param [in]		num_values	the number of values 
 *  @param [in]		values		the values 
 *	@param [out]	index		the smallest index in [0;num_values-2] for which values[index] <= value
 *	@param [out]	t			the weight for the linear interpolation s.t. value == (1-t)*values[index] + t*values[index+1] 
 * 
 *	@return 0
 */
MODELICA_NDTABLE_API void NDTable_find_index(double value, int num_values, const double values[], int *index, double *t, ModelicaNDTable_ExtrapMethod_t extrap_method);

MODELICA_NDTABLE_API int NDTable_evaluate_internal(const ModelicaNDTable_h table, const double *t, const int *subs, int *nsubs, int dim, ModelicaNDTable_InterpMethod_t interp_method, ModelicaNDTable_ExtrapMethod_t extrap_method, double *value, double *derivatives);

MODELICA_NDTABLE_API int NDTable_validate_table(ModelicaNDTable_h table);

MODELICA_NDTABLE_API int NDTable_assert_quantities_and_units(ModelicaNDTable_h table, int rank, const char *data_quantity, const char *data_unit, const char **scale_quantities, const char **scale_units);

MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_create_table(int ndims, const int *dims, const double *data, const double **scales);

/*! Calculate the number of offsets from the dimensions
 *
 *  @param [in]		ndims		the number of dimensions
 *  @param [in]		dims		the extent of the dimensions
 *	@param [out]	offs		array to write the offsets
 */
MODELICA_NDTABLE_API void NDTable_calculate_offsets(int ndims, const int dims[], int offs[]);

/*! Calculate the number of elements from the dimensions
 *
 *  @param [in]		ndims		the number of dimensions
 *  @param [in]		dims		the extent of the dimensions
 * 
 *	@return	the number of elements
 */
MODELICA_NDTABLE_API int NDTable_calculate_numel(int ndims, const int dims[]);

#ifdef __cplusplus
}
#endif

#endif /*NDTABLE_H_*/