/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <float.h>
#include <stdio.h>

#include "NDTable.h"

#ifdef _WIN32
#define ISFINITE(x) _finite(x)
#else
#define ISFINITE(x) isfinite(x)
#endif


static char error_message[MAX_MESSAGE_LENGTH] = "No error";

static ModelicaNDTable_h tables[MAX_NTABLES];

static int table_count = 0;


/* A string comparison that returns 1 if a is NULL, empty ("") or equal b and 0 otherwise */
static int streq(const char *a, const char *b) {
	if(a == NULL || strlen(a) == 0)
		return 1;

	if(b == NULL || strlen(b) == 0)
		return 0;

	return strcmp(a, b) == 0;
}

MODELICA_NDTABLE_API void NDTable_set_error_message(const char *msg, ...) {
	va_list vargs;
	va_start(vargs, msg);
	vsprintf(error_message, msg, vargs);
	va_end(vargs);
}

ModelicaNDTable_h NDTable_alloc_table() {
	return (ModelicaNDTable_h )calloc(1, sizeof(ModelicaNDTable_t));
}

void NDTable_free_table(ModelicaNDTable_h table) {
	int i;

	if(!table) return;
	
	free(table->filename);
	free(table->datasetname);
	free(table->data);
	free(table->data_quantity);
	free(table->data_unit);
	
	for(i = 0; i < MAX_NDIMS; i++) {
		free(table->scales[i]);
		free(table->scale_quantities[i]);
		free(table->scale_units[i]);
	}

	free(table);
}

int NDTable_get_table_count() {
	return table_count;
}

ModelicaNDTable_h  NDTable_get_table(int id) {
	if(id < 0 || id > MAX_NTABLES) {
		return NULL;
	}

	return tables[id];
}

int NDTable_add_table(const ModelicaNDTable_h table) {
	// check if the max number of tables has been reached
	if(table_count >= MAX_NTABLES) {
		NDTable_set_error_message("The maximum number of %d tables has been reached", MAX_NTABLES); 
		return -1;
	}

	// add the table
	tables[table_count] = (ModelicaNDTable_h) table;

	// return and increase the counter
	return table_count++;
}

void NDTable_ind2sub(const int index, const ModelicaNDTable_h table, int *subs) {
	int i, n = index; // number of remaining elements

	for(i = 0; i < table->ndims; i++) {
		subs[i] = n / table->offs[i];
		n -= subs[i] * table->offs[i];
	}
}

void NDTable_sub2ind(const int *subs, const ModelicaNDTable_h table, int *index) {
	int i, k = 1;

	(*index) = 0;

	for(i = table->ndims-1; i >= 0; i--) {
		(*index) += subs[i] * k; 
		k *= table->dims[i]; // TODO use pre-calculated offsets
	}
}

double NDTable_get_value_subs(const ModelicaNDTable_h table, const int subs[]) {
	int index;
	NDTable_sub2ind(subs, table, &index);
	return table->data[index];
}

char * NDTable_get_error_message(size_t length, char *message) {
	if(message != NULL)
		strcpy(message, error_message);

	return message;
}

MODELICA_NDTABLE_API void NDTable_calculate_offsets(int ndims, const int dims[], int *offs) {
	int i;

	if(ndims < 1) {
		return;
	}

	offs[ndims-1] = 1;

	for(i = ndims-2; i >= 0; i--) {
		offs[i] = offs[i+1] * dims[i+1];  
	}
}

MODELICA_NDTABLE_API int NDTable_calculate_numel(int ndims, const int dims[]) {
	int i, numel = 1;

	for(i = 0; i < ndims; i++) {
		numel = numel * dims[i];
	}

	return numel;
}

int NDTable_validate_table(ModelicaNDTable_h table) {
	int i, j, numel, offs[MAX_NDIMS];
	double v;

	// check the rank
	if(table->ndims < 0 || table->ndims > 32) {
		NDTable_set_error_message("The rank of '%s' in '%s' must be in the range [0;32] but was %d", table->datasetname, table->filename, table->ndims);
		return -1;
	}

	// check the extent of the dimensions
	for(i = 0; i < table->ndims; i++) {
		if(table->dims[i] < 1) {
			NDTable_set_error_message("Extent of dimension %d of '%s' in '%s' must be >=0 but was %d", i, table->datasetname, table->filename, table->dims[i]);
			return -1;
		}
	}

	// check the number of values
	numel = 1;
	for(i = 0; i < table->ndims; i++) {
		numel = numel * table->dims[i];
	}

	if(table->numel != numel) {
		NDTable_set_error_message("The size of '%s' in '%s' does not match its extent", table->datasetname, table->filename);
		return -1;
	}

	// check the offsets
	NDTable_calculate_offsets(table->ndims, table->dims, offs);
	for(i = 0; i < table->ndims; i++) {
		if(table->offs[i] != offs[i]) {
			NDTable_set_error_message("The offset[%d] of '%s' in '%s' must be %d but was %d", i, table->datasetname, table->filename, offs[i], table->offs[i]);
			return -1;
		}
	}
	
	// check the scales
	for(i = 0; i < table->ndims; i++) {

		// make sure a scale is set
		if(table->scales[i] == NULL) {
			NDTable_set_error_message("Scale for dimension %d of '%s' in '%s' is not set", i, table->datasetname, table->filename);
			return -1;
		}

		// check strict monotonicity
		v = table->scales[i][0];
		for(j = 1; j < table->dims[i]; j++) {
			if(v >= table->scales[i][j]) {
				NDTable_set_error_message("Scale for dimension %d of '%s' in '%s' is not strictly monotonic increasing at index %d", i, table->datasetname, table->filename, j);
				return -1;
			}
			v = table->scales[i][j];
		}
		
		if(table->offs[i] != offs[i]) {
			NDTable_set_error_message("The offset[%d] of '%s' in '%s' must be %d but was %d", i, table->datasetname, table->filename, offs[i], table->offs[i]);
			return -1;
		}
	}

	// check the data for non-finite values
	for(i = 0; i < table->numel; i++) {
		if(!ISFINITE(table->data[i])) {
			NDTable_set_error_message("The data value at index %d of '%s' in '%s' is not finite", i, table->datasetname, table->filename);
			return -1;
		}
	}

	return 0;
}

MODELICA_NDTABLE_API int NDTable_assert_quantities_and_units(ModelicaNDTable_h table, int rank, const char *data_quantity, const char *data_unit, const char **scale_quantities, const char **scale_units) {
	int i;

	// check the rank
	if(table->ndims != rank) {
		NDTable_set_error_message("Dataset '%s' in '%s' has the wrong number of dimensions. Expected %d but was %d.", table->datasetname, table->filename, rank, table->ndims);
		return -1;
	}
	
	if(!streq(data_quantity, table->data_quantity)) {
		NDTable_set_error_message("Dataset '%s' in '%s' has the wrong quantity. Expected '%s' but was '%s'.", table->datasetname, table->filename, data_quantity, table->data_quantity);
		return -1;
	}

	if(!streq(data_unit, table->data_unit)) {
		NDTable_set_error_message("Dataset '%s' in '%s' has the wrong unit. Expected '%s' but was '%s'.", table->datasetname, table->filename, data_unit, table->data_unit);
		return -1;
	}

	if(scale_quantities != NULL) {
		for(i = 0; i < table->ndims; i++) {
			if(!streq(scale_quantities[i], table->scale_quantities[i])) {
				NDTable_set_error_message("The scale for dimension %d of dataset '%s' in '%s' has the wrong quantity. Expected '%s' but was '%s'.", 
					i, table->datasetname, table->filename, scale_quantities[i], table->scale_quantities[i]);
				return -1;
			}
		}
	}

	if(scale_units != NULL) {
		for(i = 0; i < table->ndims; i++) {
			if(!streq(scale_units[i], table->scale_units[i])) {
				NDTable_set_error_message("The scale for dimension %d of dataset '%s' in '%s' has the wrong unit. Expected '%s' but was '%s'.", i, table->datasetname, table->filename, scale_units[i], table->scale_units[i]);
				return -1;
			}
		}
	}

	return 0;
}

MODELICA_NDTABLE_API ModelicaNDTable_h NDTable_create_table(int ndims, const int *dims, const double *data, const double **scales) {
	int i, j;
	ModelicaNDTable_h table = NDTable_alloc_table();

	table->ndims = ndims;
	table->numel = NDTable_calculate_numel(table->ndims, table->dims);
	NDTable_calculate_offsets(table->ndims, table->dims, table->offs);

	table->data = (double *)malloc(sizeof(double) * table->numel);
	for(i = 0; i < table->numel; i++) {
		table->data[i] = data[i];
	}

	for(i = 0; i < ndims; i++) {
		table->dims[i] = dims[i];
		table->scales[i] = (double *)malloc(sizeof(double) * dims[i]);
		for(j = 0; j < dims[i]; j++) {
			table->scales[i][j] = scales[i][j];
		}
	}

	return table;
}
