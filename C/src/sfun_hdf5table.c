/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#define S_FUNCTION_NAME sfun_hdf5table
#define S_FUNCTION_LEVEL 2

#include "simstruc.h"
#include "NDTable.h"


#define MAX_MESSAGE_LENGTH 256

static char		message[MAX_MESSAGE_LENGTH];


static void mdlInitializeSizes(SimStruct *S) {
	const mxArray * pa_ndims	= NULL;
	mxChar *		str			= NULL;
	size_t			i			= 0;
	size_t			nchars		= 0;
	int				ndims		= -1;
	
	ssSetNumSFcnParams(S, 10);
	if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
		return; // Parameter mismatch reported by the Simulink engine
	}

	ssSetNumIWork(S, 3);
	ssSetNumPWork(S, 1);

	// get the number of dimensions
	pa_ndims = ssGetSFcnParam(S, 2);
	if(mxIsNumeric(pa_ndims)) {
		ndims = (int) mxGetScalar(pa_ndims); 
	} else {
		sprintf(message, "Parameter 3 must be an int specifying the number of dimensions.");
		ssSetErrorStatus(S, message);
	}

	if(ndims < 0 || ndims > 32) {
		sprintf(message, "The number of dimensions must be between 0 and 32.");
		ssSetErrorStatus(S, message);
	}

	if (!ssSetNumInputPorts(S, ndims)) return;
	for(i = 0; i < (size_t)ndims; i++) {
		ssSetInputPortWidth(S, i, 1);
		ssSetInputPortDirectFeedThrough(S, i, 1);
	}

	if (!ssSetNumOutputPorts(S,1)) return;
	ssSetOutputPortWidth(S, 0, 1);

	ssSetNumSampleTimes(S, 1);

	/* Take care when specifying exception free code - see sfuntmpl.doc */
	ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

static void mdlInitializeSampleTimes(SimStruct *S) {
	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
	ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
#if defined(MDL_START) 
 static void mdlStart(SimStruct *S)
 {

	const mxArray * pa_filename			= NULL;
	const mxArray * pa_dataset_name		= NULL;
	const mxArray * pa_ndims			= NULL;
	const mxArray * pa_dataset_quantity	= NULL;
	const mxArray * pa_dataset_unit		= NULL;
	const mxArray * pa_scale_quantities	= NULL;
	const mxArray * pa_scale_units		= NULL;
	const mxArray * pa_interp_method	= NULL;
	const mxArray * pa_extrap_method	= NULL;
	const mxArray * pa_share			= NULL;
	const mxArray * pa_cell				= NULL;
	mxChar *		str					= NULL;
	int				i					= 0;
	size_t			nchars				= 0;
	size_t			ncells				= 0;
	char *			filename			= NULL; 
	char *			dataset_name		= NULL;
	char *			dataset_quantity	= NULL;
	char *			dataset_unit		= NULL;
	char *			scale_quantities[32]= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	char *			scale_units[32]		= {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	char *			interp_method_str	= NULL;
	char *			extrap_method_str	= NULL;
	int				share				= -1;
	/*
	int	*			ndims				= NULL;
	int *			dsid				= NULL;
	int *			interp_method		= NULL;
	int *			extrap_method		= NULL;
	*/

	int_T *			ndims				= ssGetIWork(S);
	//int_T *			dsid				= ndims + 1;
	int_T *			interp_method		= ndims + 1;
	int_T *			extrap_method		= ndims + 2;

	ModelicaNDTable_h *		dataset				= ssGetPWork(S);

	//memset(scale_quantities, NULL, sizeof(scale_quantities));
	//memset(scale_units, NULL, sizeof(scale_units));

	// get the file name
	pa_filename = ssGetSFcnParam(S, 0);
	if(mxIsChar(pa_filename)) {
		nchars = mxGetNumberOfElements(pa_filename) + 1;
		filename = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_filename, filename, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 1 must be a string");
		goto out;
	}

	// get the dataset name
	pa_dataset_name = ssGetSFcnParam(S, 1);
	if(mxIsChar(pa_dataset_name)) {
		nchars = mxGetNumberOfElements(pa_dataset_name) + 1;
		dataset_name = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_dataset_name, dataset_name, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 2 must be a string");
		goto out;
	}
	
	// get the number of dimensions
	pa_ndims = ssGetSFcnParam(S, 2);
	if(mxIsNumeric(pa_ndims)) {
		ndims[0] = (int) mxGetScalar(pa_ndims);
	} else {
		sprintf(message, "Parameter 3 must be an int specifying the number of dimensions.");
		ssSetErrorStatus(S, message);
		goto out;
	}

	// get the dataset quantity
	pa_dataset_quantity = ssGetSFcnParam(S, 3);
	if(mxIsChar(pa_dataset_quantity)) {
		nchars = mxGetNumberOfElements(pa_dataset_quantity) + 1;
		dataset_quantity = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_dataset_quantity, dataset_quantity, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 4 must be a string");
		goto out;
	}

	// get the dataset unit
	pa_dataset_unit = ssGetSFcnParam(S, 4);
	if(mxIsChar(pa_dataset_unit)) {
		nchars = mxGetNumberOfElements(pa_dataset_unit) + 1;
		dataset_unit = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_dataset_unit, dataset_unit, nchars);
	} else {
		ssSetErrorStatus(S, "Parameter 5 must be a string");
		goto out;
	}

	// get the scale quantities
	pa_scale_quantities = ssGetSFcnParam(S, 5);
	if(mxIsCell(pa_scale_quantities)) {
		ncells = mxGetNumberOfElements(pa_scale_quantities);
		if(ncells != ndims[0]) {
			ssSetErrorStatus(S, "Parameter 5 must contain one string for every dimension");
			goto out;
		}
		for(i = 0; i < (int)ncells; i++) {
			pa_cell = mxGetCell(pa_scale_quantities, i);
			if(mxIsChar(pa_cell)) {
				nchars = mxGetNumberOfElements(pa_cell) + 1;
				scale_quantities[i] = (char *) mxCalloc(nchars, sizeof(char));
				mxGetString(pa_cell, scale_quantities[i], nchars);
			} else {
				ssSetErrorStatus(S, "Parameter 6 contains non-string values");
				goto out;
			}
		}
	} else {
		ssSetErrorStatus(S, "Parameter 6 must be a cell array of strings");
		goto out;
	}

	// get the scale units
	pa_scale_units = ssGetSFcnParam(S, 6);
	if(mxIsCell(pa_scale_units)) {
		ncells = mxGetNumberOfElements(pa_scale_units);
		if(ncells != ndims[0]) {
			ssSetErrorStatus(S, "Parameter 7 must contain one string for every dimension");
			goto out;
		}
		for(i = 0; i < (int)ncells; i++) {
			pa_cell = mxGetCell(pa_scale_units, i);
			if(mxIsChar(pa_cell)) {
				nchars = mxGetNumberOfElements(pa_cell) + 1;
				scale_units[i] = (char *) mxCalloc(nchars, sizeof(char));
				mxGetString(pa_cell, scale_units[i], nchars);
			} else {
				ssSetErrorStatus(S, "Parameter 7 contains non-string values");
				goto out;
			}
		}
	} else {
		ssSetErrorStatus(S, "Parameter 7 must be a cell array of strings");
		goto out;
	}

	// get the intepolation method
	pa_interp_method = ssGetSFcnParam(S, 7);
	if(mxIsChar(pa_interp_method)) {
		nchars = mxGetNumberOfElements(pa_interp_method) + 1;
		interp_method_str = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_interp_method, interp_method_str, nchars);
		
		if (_stricmp(interp_method_str, "Hold") == 0) {
			interp_method[0] = NDTABLE_INTERP_HOLD;
		} else if (_stricmp(interp_method_str, "Linear") == 0) {
			interp_method[0] = NDTABLE_INTERP_LINEAR;
		}  else if (_stricmp(interp_method_str, "Akima") == 0) {
			interp_method[0] = NDTABLE_INTERP_AKIMA;
		} else {
			ssSetErrorStatus(S, "Parameter 8 must be one of 'Hold', 'Linear' or 'Akima'");
			goto out;
		}
	} else {
		ssSetErrorStatus(S, "Parameter 8 must be a string");
		goto out;
	}

	// get the extrapolation method
	pa_extrap_method = ssGetSFcnParam(S, 8);
	if(mxIsChar(pa_extrap_method)) {
		nchars = mxGetNumberOfElements(pa_extrap_method) + 1;
		extrap_method_str = (char *) mxCalloc(nchars, sizeof(char));
		mxGetString(pa_extrap_method, extrap_method_str, nchars);
		
		if (_stricmp(extrap_method_str, "Hold") == 0) {
			extrap_method[0] = NDTABLE_EXTRAP_HOLD;
		} else if (_stricmp(extrap_method_str, "Linear") == 0) {
			extrap_method[0] = NDTABLE_EXTRAP_LINEAR;
		} else if (_stricmp(extrap_method_str, "None") == 0) {
			extrap_method[0] = NDTABLE_EXTRAP_NONE;
		} else {
			ssSetErrorStatus(S, "Parameter 9 must be one of 'Hold', 'Linear', 'Loop', 'PingPong' or 'None'");
			goto out;
		}
	} else {
		ssSetErrorStatus(S, "Parameter 9 must be a string");
		goto out;
	}

	// get the share flag
	pa_share = ssGetSFcnParam(S, 9);
	if(mxIsNumeric(pa_share)) {
		share = (int) mxGetScalar(pa_share);
	} else {
		sprintf(message, "Parameter 3 must be an int specifying whether to share the dataset.");
		ssSetErrorStatus(S, message);
		goto out;
	}

	*dataset = NDTable_read(filename, dataset_name, ndims[0], dataset_quantity, dataset_unit, scale_quantities, scale_units, share);

	if(*dataset == NULL) {
		ssSetErrorStatus(S, NDTable_get_error_message(MAX_MESSAGE_LENGTH, message));
		goto out;
	}

out:
	// free allocated memory
	if(filename)		 { mxFree(filename); }
	if(dataset_name)	 { mxFree(dataset_name); }
	if(dataset_quantity) { mxFree(dataset_quantity); }
	if(dataset_unit)	 { mxFree(dataset_unit); }
	for(i = 0; i < 32; i++) {
		if(scale_quantities[i]) { mxFree(scale_quantities[i]); }
		if(scale_units[i])		{ mxFree(scale_units[i]); }
	}
	if(interp_method_str)	 { mxFree(interp_method_str); }
	if(interp_method_str)	 { mxFree(extrap_method_str); }
}
#endif /*  MDL_START */


static void mdlOutputs(SimStruct *S, int_T tid) {
	double params[32];
	int i;
	InputRealPtrsType uPtrs;
	real_T *y = ssGetOutputPortRealSignal(S,0);
	
	int_T ndims			= ssGetIWork(S)[0];
	//int_T dsid			= ssGetIWork(S)[1];
	int_T interp_method	= ssGetIWork(S)[1];
	int_T extrap_method	= ssGetIWork(S)[2];
	ModelicaNDTable_h dataset	= ssGetPWork(S)[0];

	for(i = 0; i < ndims; i++) {
		uPtrs = ssGetInputPortRealSignalPtrs(S, i);
		params[i] = *uPtrs[0];
	}

	if (NDTable_evaluate(dataset, ndims, params, (ModelicaNDTable_InterpMethod_t)interp_method, (ModelicaNDTable_ExtrapMethod_t)extrap_method, y) != 0) {
		ssSetErrorStatus(S, NDTable_get_error_message(MAX_MESSAGE_LENGTH, message));
	}
}

/*
#define MDL_DERIVATIVES
static void mdlDerivatives(SimStruct *S)
{
    real_T            *dx   = ssGetdX(S);
    real_T            *x    = ssGetContStates(S);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);

    // xdot=Ax+Bu
    dx[0]=A[0][0]*x[0]+A[0][1]*x[1]+B[0][0]*U(0)+B[0][1]*U(1);
    dx[1]=A[1][0]*x[0]+A[1][1]*x[1]+B[1][0]*U(0)+B[1][1]*U(1);
}
*/


static void mdlTerminate(SimStruct *S) {
	UNUSED_ARG(S); /* unused input argument */
	// TODO: close table
}


#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
#include "simulink.c" /* MEX-file interface mechanism */
#else
#include "cg_sfun.h" /* Code generation registration function */
#endif
