/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "hdf5.h"
#include "hdf5_hl.h"

#include "NDTable.h"

// dataset names for recording
#define TIME				"time"
#define SIGNAL_NAMES		"signal_names"
#define SIGNAL_QUANTITIES	"signal_quantities"
#define SIGNAL_UNITS		"signal_units"
#define SIGNAL_VALUES		"signal_values"
#define SIGNAL_COMMENTS		"signal_comments"

#define ASSERT_NO_ERROR(_ret)	if((_ret) < 0)	{ status = -1; goto out; }
#define ASSERT_VALID_ID(_id)	if((_id) < 0)	{ status = -1; goto out; }
#define ASSERT_TRUE(_cond)		if(!(_cond))	{ status = -1; goto out; }

herr_t iterateScales(hid_t did, unsigned dim, hid_t scale, ModelicaNDTable_h dataset);

static void free_string_array(size_t len, char **arr) {
	size_t i;
	for(i = 0; i < len; i++) free(arr[i]);
	free(arr);
}

static int get_string_attribute(hid_t loc_id, const char *obj_name, const char *attr_name, char **attr_value) {
	herr_t status = 0;
	H5T_class_t type_class = H5T_NO_CLASS;
	size_t type_size = 0;

	// check if the attribute exists
	if(!H5Aexists_by_name(loc_id, obj_name, attr_name, H5P_DEFAULT)) {
		return -1;
	}

	if (H5LTget_attribute_info(loc_id, obj_name, attr_name, NULL, &type_class, &type_size) != 0) {
		return -1;
	}

	if (type_class != H5T_STRING || type_size < 1) {
		return -1;
	}

	*attr_value = (char *)calloc(type_size + 1, sizeof(char));

	if(H5LTget_attribute_string(loc_id, obj_name, attr_name, *attr_value) != 0) {
		return -1;
	}

	return status;
}


ModelicaNDTable_h NDTable_read(const char *filename, const char *datasetname, const int ndims, const char *data_quantity, const char *data_unit, const char **scale_quantities, const char **scale_units, int share) {
	hid_t       file_id = H5I_INVALID_HID,
                ds_id = H5I_INVALID_HID;
	int			i = -1, status = 0, nscales = -1;
	ModelicaNDTable_h	ds = NULL;
	H5T_class_t clazz = H5T_NO_CLASS;
	size_t		size = 0;
	hsize_t		dims_buf[MAX_NDIMS];

	if(filename == NULL) {
		NDTable_set_error_message("The file name must not be NULL");
		return NULL;
	}

	if(datasetname == NULL) {
		NDTable_set_error_message("The dataset name must not be NULL");
		return NULL;
	}

	if(ndims < 0 || ndims > MAX_NDIMS) {
		NDTable_set_error_message("The number of dimensions must be in the range [0; %d]", MAX_NDIMS);
		return NULL;
	}

	if(share) {

		// try the usertabs first
		if((ds = NDTable_find_usertab(filename, datasetname)) != NULL) {
			goto out;
		}

		for(i = 0; i < NDTable_get_table_count(); i++) {
			ds = NDTable_get_table(i);
			
			if(ds->filename && strcmp(filename, ds->filename) == 0 && ds->datasetname && strcmp(datasetname, ds->datasetname) == 0) {
				
				// check the quantities and units
				if(NDTable_assert_quantities_and_units(ds, ndims, data_quantity, data_unit, scale_quantities, scale_units) != 0) {
					// NDTable_assert_quantities_and_units() will set the error message
					return NULL;	
				}

				return ds;
			}
		}
	}

	ds = NDTable_alloc_table();

	// save the file and dataset name
	ds->filename = strdup(filename);
    ds->datasetname = strdup(datasetname);
	
	// check if file is HDF5
	// H5Fis_hdf5()

	// open the file
	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		NDTable_set_error_message("Failed to open '%s'", filename);
		status = -1;
		goto out;
	}

	// check if the dataset exists
	if(H5Lexists(file_id, datasetname, H5P_DEFAULT) < 1) {
		NDTable_set_error_message("Dataset '%s' does not exist in '%s'", datasetname, filename);
		status = -1;
		goto out;
	}

	// get the rank
	if (H5LTget_dataset_ndims(file_id, datasetname, &ds->ndims) < 0) {
		NDTable_set_error_message(
			"Cannot find dataset '%s' in '%s'", datasetname, filename);
		status = -1;
		goto out;
	}

	// get the dimensions, class and (type) size of the dataset
	if (H5LTget_dataset_info(file_id, datasetname, dims_buf, &clazz, &size) < 0) {
		NDTable_set_error_message(
			"Failed to retrieve the dimensions for dataset '%s' in '%s'", datasetname, filename);
		status = -1;
		goto out;
	}

	// copy the dimensions
	for (i = 0; i < ds->ndims; i++) {
		ds->dims[i] = (int) dims_buf[i];
	}

	ds->numel = 1;
	for (i = 0; i < ds->ndims; i++) {
		ds->numel = ds->numel * ds->dims[i];
	}

	
	if (ds->numel == 1) { // if the dataset has only value it is 0D
		ds->ndims = 0;
	} else if (ndims != ds->ndims) { // check if the rank matches the table's dimensions
		NDTable_set_error_message(
			"Dataset '%s' in '%s' has the wrong number of dimensions. Expected %d but was %d.", datasetname, filename, ndims, ds->ndims);
		status = -1;
		goto out;
	}

	// check the type
	if (clazz != H5T_FLOAT) {
		NDTable_set_error_message(
			"Dataset '%s' in '%s' has the wrong type. Expected H5T_FLOAT = 5 but was %d.", datasetname, filename, clazz);
		status = -1;
		goto out;
	}

	// check the type size
	if (size != sizeof(double)) {
		NDTable_set_error_message(
			"Dataset '%s' in '%s' has the wrong type size. Expected 8 bytes but was %d.", datasetname, filename, size);
		status = -1;
		goto out;
	}

	// save the data quantity
	get_string_attribute(file_id, datasetname, QUANTITY_ATTR_NAME, &ds->data_quantity);
	get_string_attribute(file_id, datasetname, UNIT_ATTR_NAME, &ds->data_unit);

	ds->data = (double *)calloc(ds->numel, sizeof(double));

	// read dataset
	status = H5LTread_dataset_double(file_id, datasetname, ds->data);

	if (ds->ndims > 0) {
		// get the dataset id
		if ((ds_id = H5Oopen(file_id, datasetname, H5P_DEFAULT)) < 0) {
			NDTable_set_error_message("Failed to open dataset '%s' in '%s'", datasetname, filename);
			status = -1;
			goto out;
		}

		// cacuclate the offsets
		ds->offs[ds->ndims - 1] = 1;
		for (i = ds->ndims - 2; i >= 0; i--) {
			ds->offs[i] = ds->offs[i + 1] * ds->dims[i + 1];
		}

		// ...and iterate over the scales
		for (i = 0; i < ds->ndims; i++) {
			if ((nscales = H5DSget_num_scales(ds_id, i)) != 1) {
				NDTable_set_error_message("Dataset '%s' in '%s' has the wrong number of scales for dimension %d. Expected 1 but was %d.", datasetname, filename, i, nscales);
				status = -1;
				goto out;
			}

			if (H5DSiterate_scales(ds_id, i, NULL, (H5DS_iterate_t)iterateScales, ds) < 0) {
				// iterateScales() will set the error message
				status = -1;
				goto out;
			}
		}

	}

out:
	// close the handles
	if (ds_id > 0)
		H5Oclose(ds_id);

	if (file_id > 0)
		H5Fclose(file_id);

	if (status != 0) {
		return NULL;
	} 
	
	// check the consistency
	if(NDTable_validate_table(ds) != 0) {
		// NDTable_validate_table() will set the error message
		return NULL;	
	}

	// check the quantities and units
	if(NDTable_assert_quantities_and_units(ds, ndims, data_quantity, data_unit, scale_quantities, scale_units) != 0) {
		// NDTable_assert_quantities_and_units() will set the error message
		return NULL;	
	}

	// increment the dataset count
	NDTable_add_table(ds);

	return ds;
}

herr_t iterateScales(hid_t did, unsigned dim, hid_t scale, ModelicaNDTable_h dataset) {
	hid_t sid = H5I_INVALID_HID, tid = H5I_INVALID_HID, fid = H5I_INVALID_HID;
	int rank = -1;
	hsize_t length;
	double * data;
	char name_buffer[1024];

	// get the dataspace handle
	if ((sid = H5Dget_space(scale)) < 0)
		goto out;

	// get the rank
	if ((rank = H5Sget_simple_extent_ndims(sid)) < 0)
		goto out;

	// get the dimensions
	if (H5Sget_simple_extent_dims(sid, &length, NULL) < 0)
		goto out;

	if (length != dataset->dims[dim])
		goto out;

	if ((tid = H5Dget_type(did)) < 0)
		goto out;

	// get the class
	if (H5Tget_class(tid) != H5T_FLOAT)
		goto out;

	// get the size
	if (H5Tget_size(tid) != sizeof(double))
		goto out;

	// Terminate access to the dataspace
	if (H5Sclose(sid) < 0)
		goto out;

	data = (double *)calloc((size_t)length, sizeof(double));

	// read
	if (H5Dread(scale, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data) < 0)
		goto out;

	fid = H5Iget_file_id(scale);
	H5Iget_name(scale, name_buffer, 1024);

	// save the quantity
	get_string_attribute(scale, ".", QUANTITY_ATTR_NAME, &dataset->scale_quantities[dim]);

	// save the unit
	get_string_attribute(scale, ".", UNIT_ATTR_NAME, &dataset->scale_units[dim]);

	dataset->scales[dim] = data;

	return H5_ITER_STOP;

out:
	return H5_ITER_ERROR;
}

int NDTable_get_dataset_rank(const char *filename, const char *dataset_name){
	hid_t file_id = H5I_INVALID_HID;
	int ndims;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		goto out;
	}

	if (H5LTget_dataset_ndims(file_id, dataset_name, &ndims) != 0) {
		ndims = -1;
		goto out;
	}

out:
	if (file_id >= 0) {
		H5Fclose(file_id);
	}

	return ndims;
}

int NDTable_get_dataset_info(const char *filename, const char *dataset_name, int *ndims, int dims[]) {
	hid_t file_id = H5I_INVALID_HID;
	H5T_class_t type_class;
	size_t size;
	hsize_t dimsbuf[32];
	int i;
	int status = 0;

	if ((file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) {
		status = -1;
		goto out;
	}

	if (H5LTget_dataset_ndims(file_id, dataset_name, ndims) != 0) {
		status = -1;
		goto out;
	}

	if (dims == NULL) {
		goto out;
	}

	if (H5LTget_dataset_info(file_id, dataset_name, dimsbuf, &type_class, &size) != 0) {
		status = -1;
		goto out;
	}

	for (i = 0; i < *ndims; i++) {
		dims[i] = (int)dimsbuf[i];
	}

out:
	if (file_id >= 0) {
		H5Fclose(file_id);
	}

	return status;
}


typedef struct {
	// general
	hid_t		file;
	
	// time
	hid_t		time_dataspace;
	hid_t		time_dataset;
	hsize_t		time_dims[1];		
    hsize_t     time_size[1];
	hsize_t     time_offset[1];

	// signals
	hid_t		real_dataspace;
	hid_t		real_dataset;
	hsize_t		real_dims[2];		
    hsize_t     real_size[2];
    hsize_t     real_offset[2];
} recorder;

static recorder rec;


int save_strings(hid_t file, hsize_t dims[], hid_t filetype, hid_t memtype, const char *dset_name, const char **strings) {
	hid_t dset, space;
	herr_t status;
	
	// Create dataspace.  Setting maximum size to NULL sets the maximum size to be the current size.
    space = H5Screate_simple (1, dims, NULL);

    // Create the dataset and write the variable-length string data to
    dset = H5Dcreate (file, dset_name, filetype, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	status = H5Dwrite (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, strings);
	
	status = H5Sclose (space);
	status = H5Dclose (dset);

    return status;
}

int NDTable_open_recorder(const char* filename, int num_signals, const char **signal_names, const char **signal_quantities, const char **signal_units, const char **signal_comments) {

	hid_t	 filetype = H5I_INVALID_HID;
	hid_t	 memtype  = H5I_INVALID_HID;
	hid_t	 prop	  = H5I_INVALID_HID;
	hsize_t  names_dims[1];
	hsize_t  time_maxdims[1];
	hsize_t  real_maxdims[2];
	herr_t   status = 0;

	// names
	names_dims[0] = num_signals;

	// time
	time_maxdims[0] = H5S_UNLIMITED;
	rec.time_dims[0] = 1;
	rec.time_offset[0] = 0;

	// signals
	rec.real_dims[0] = 1;
	rec.real_dims[1] = num_signals;
	real_maxdims[0] = H5S_UNLIMITED;
	real_maxdims[1] = num_signals;	
	rec.real_offset[0] = 0;
	rec.real_offset[1] = 0;

    // create a new file (if the file exists its contents will be overwritten)
    if((rec.file = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)) < 0) { status = -1; goto out; }

	// Create file and memory datatypes
	if((filetype = H5Tcopy (H5T_FORTRAN_S1)) < 0) { status = -1; goto out; }
    if(H5Tset_size (filetype, H5T_VARIABLE) != 0) { status = -1; goto out; }
    if((memtype = H5Tcopy (H5T_C_S1)) < 0)		  { status = -1; goto out; }
    if(H5Tset_size (memtype, H5T_VARIABLE) != 0)  { status = -1; goto out; }

	if(save_strings(rec.file, names_dims, filetype, memtype, SIGNAL_NAMES, signal_names) != 0)		   { status = -1; goto out; }
	if(save_strings(rec.file, names_dims, filetype, memtype, SIGNAL_QUANTITIES, signal_quantities) != 0) { status = -1; goto out; }
	if(save_strings(rec.file, names_dims, filetype, memtype, SIGNAL_UNITS, signal_units) != 0)		   { status = -1; goto out; }
	if(save_strings(rec.file, names_dims, filetype, memtype, SIGNAL_COMMENTS, signal_comments) != 0)	   { status = -1; goto out; }

	if(H5Tclose (filetype) != 0) { status = -1; goto out; }
    if(H5Tclose (memtype) != 0)  { status = -1; goto out; }

	// time
	if((rec.time_dataspace = H5Screate_simple (1, rec.time_dims, time_maxdims)) < 0) { status = -1; goto out; }
	if((prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) { status = -1; goto out; }
	if(H5Pset_chunk (prop, 1, rec.time_dims) != 0)  { status = -1; goto out; }
	if((rec.time_dataset = H5Dcreate2 (rec.file, TIME, H5T_NATIVE_DOUBLE, rec.time_dataspace, H5P_DEFAULT, prop, H5P_DEFAULT)) < 0) { status = -1; goto out; } 
	if(H5Pclose (prop) != 0) { status = -1; goto out; }

	// signals
    if((rec.real_dataspace = H5Screate_simple (2, rec.real_dims, real_maxdims)) < 0) { status = -1; goto out; }
	if((prop = H5Pcreate (H5P_DATASET_CREATE)) < 0) { status = -1; goto out; }
    if(H5Pset_chunk (prop, 2, rec.real_dims) != 0)  { status = -1; goto out; }
	if((rec.real_dataset = H5Dcreate2 (rec.file, SIGNAL_VALUES, H5T_NATIVE_DOUBLE, rec.real_dataspace, H5P_DEFAULT, prop, H5P_DEFAULT)) < 0) { status = -1; goto out; }
	if(H5Pclose (prop) != 0) { status = -1; goto out; }

out:
	return status;
}

int NDTable_record_signals(double time, int num_signals, const double *signal_values) {
	herr_t	status = 0;
	hid_t	memspace  = H5I_INVALID_HID;
	hid_t	dataspace = H5I_INVALID_HID;

	rec.time_size[0] = rec.time_offset[0] + rec.time_dims[0];

	if(H5Dset_extent (rec.time_dataset, rec.time_size) != 0) { status = -1; goto out; }
	if((dataspace = H5Dget_space (rec.time_dataset)) < 0) { status = -1; goto out; }
	if(H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, rec.time_offset, NULL, rec.time_dims, NULL) != 0) { status = -1; goto out; } 
	if((memspace = H5Screate_simple (1, rec.time_dims, NULL)) < 0) { status = -1; goto out; }
	if(H5Dwrite (rec.time_dataset, H5T_NATIVE_DOUBLE, memspace, dataspace, H5P_DEFAULT, &time) != 0) { status = -1; goto out; }
	if(H5Sclose (memspace) != 0) { status = -1; goto out; }
	if(H5Sclose(dataspace) != 0) { status = -1; goto out; }
	rec.time_offset[0]++;

	// extend the dataset
	rec.real_size[0] = rec.real_offset[0] + rec.real_dims[0];
	rec.real_size[1] = rec.real_dims[1];

	if(H5Dset_extent (rec.real_dataset, rec.real_size) != 0) { status = -1; goto out; }
	
	// select a hyperslab in extended portion of dataset
	if((dataspace = H5Dget_space (rec.real_dataset)) < 0) { status = -1; goto out; }
	if(H5Sselect_hyperslab (dataspace, H5S_SELECT_SET, rec.real_offset, NULL, rec.real_dims, NULL) != 0) { status = -1; goto out; }
	if((memspace = H5Screate_simple (2, rec.real_dims, NULL)) < 0) { status = -1; goto out; }
	if(H5Dwrite (rec.real_dataset, H5T_NATIVE_DOUBLE, memspace, dataspace, H5P_DEFAULT, signal_values) != 0) { status = -1; goto out; }
	if(H5Sclose (memspace) != 0) { status = -1; goto out; }
	if(H5Sclose(dataspace) != 0) { status = -1; goto out; }
	rec.real_offset[0]++;

out:
	return status;
}

int NDTable_close_recorder() {
	herr_t status = 0;
	
	if(H5Dclose (rec.time_dataset) != 0)	{ status = -1; }
    if(H5Sclose (rec.time_dataspace) != 0)	{ status = -1; }
	if(H5Dclose (rec.real_dataset) != 0)	{ status = -1; }
    if(H5Sclose (rec.real_dataspace) != 0)	{ status = -1; }
    if(H5Fclose (rec.file) != 0)			{ status = -1; }

	return status;
}

static char ** read_strings(hid_t file, hsize_t *nvalues, const char* dataset_name) {
	hid_t       filetype	= H5I_INVALID_HID;
	hid_t       memtype		= H5I_INVALID_HID;
	hid_t       space		= H5I_INVALID_HID;
	hid_t       dset		= H5I_INVALID_HID;                                          
    herr_t      status		= 0;
    char **		rdata		= NULL; // return buffer
	char **		rbuf		= NULL; // temporary read buffer
    int         ndims, i;

	ASSERT_VALID_ID(dset = H5Dopen (file, dataset_name, H5P_DEFAULT))

    // get the datatype
    ASSERT_VALID_ID(filetype = H5Dget_type (dset))

    // get dataspace and allocate memory for read buffer
	ASSERT_VALID_ID(space = H5Dget_space (dset))
    ndims = H5Sget_simple_extent_dims (space, nvalues, NULL);
	rbuf = (char **) malloc ((*nvalues) * sizeof (char *));
	rdata = (char **) malloc ((*nvalues) * sizeof (char *));

    // create the memory datatype
    ASSERT_VALID_ID(memtype = H5Tcopy (H5T_C_S1))
    ASSERT_NO_ERROR(H5Tset_size (memtype, H5T_VARIABLE))

    // read the data
    ASSERT_NO_ERROR(H5Dread (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf))

	// copy the strings
	for(i = 0; i < *nvalues; i++) {
		rdata[i] = strdup(rbuf[i]);
	}

out:
    /*
     * Close and release resources.  Note that H5Dvlen_reclaim works
     * for variable-length strings as well as variable-length arrays.
     * Also note that we must still free the array of pointers stored
     * in rdata, as H5Tvlen_reclaim only frees the data these point to.
     */
    H5Dvlen_reclaim (memtype, space, H5P_DEFAULT, rbuf);
    H5Dclose (dset);
    H5Sclose (space);
    H5Tclose (filetype);
    H5Tclose (memtype);

    return (status == 0) ? rdata : NULL;
}


int NDTable_result2sdf(const char* in_file, const char* out_file) {
	hid_t       fid_result	= H5I_INVALID_HID;
	hid_t       space		= H5I_INVALID_HID;
	hid_t       dset		= H5I_INVALID_HID;
	hid_t       fid_sdf		= H5I_INVALID_HID;
	hid_t       ds_time		= H5I_INVALID_HID;
	hid_t       ds_signal	= H5I_INVALID_HID;
	hid_t       memspace	= H5I_INVALID_HID;

    herr_t      status		= 0;
    hsize_t     dims[2]		= { 0, 0 };
	hsize_t     start[2]	= { 0, 0 };
	hsize_t     count[2]	= { 0, 1 };
	H5T_class_t class_id			= H5T_NO_CLASS;
	size_t		type_size			= -1;
    char **		signal_names		= NULL;
	char **		signal_quantities	= NULL;
	char **		signal_units		= NULL;
	char **		signal_comments		= NULL;
    int         i;
	int			rank;
	hsize_t		nvalues;
	hsize_t		nsignals;
	hsize_t		nsamples; 
	double *	time_data			= NULL;
	double *	signals_data		= NULL;
	double *	buffer				= NULL;

    // open the result file
	ASSERT_VALID_ID(fid_result = H5Fopen (in_file, H5F_ACC_RDONLY, H5P_DEFAULT))

	// open the time dataset
	ASSERT_NO_ERROR(H5LTget_dataset_ndims (fid_result, TIME, &rank))

	ASSERT_TRUE(rank == 1)

	// TODO: check rank
	ASSERT_NO_ERROR(H5LTget_dataset_info (fid_result, TIME, &nsamples, &class_id, &type_size))
	
	// TODO: check class & size
	ASSERT_TRUE(nsamples > 0)
	ASSERT_TRUE(class_id == H5T_FLOAT)
	ASSERT_TRUE(type_size == 8)

	time_data = (double *) malloc(nsamples * sizeof(double)); 

	ASSERT_NO_ERROR(H5LTread_dataset_double(fid_result, TIME, time_data))

	// open the signals dataset
	ASSERT_NO_ERROR(H5LTget_dataset_ndims (fid_result, SIGNAL_VALUES, &rank))
	
	// check rank
	ASSERT_TRUE(rank == 2)

	status = H5LTget_dataset_info (fid_result, SIGNAL_VALUES, dims, &class_id, &type_size);
	// check size and type
	ASSERT_TRUE(dims[0] == nsamples)
	ASSERT_TRUE(dims[1] >= 1)
	ASSERT_TRUE(class_id == H5T_FLOAT)
	ASSERT_TRUE(type_size == 8)

	nsignals = dims[1];

	signal_names = read_strings(fid_result, &nvalues, SIGNAL_NAMES);
	ASSERT_TRUE(nvalues == nsignals)

	signal_quantities = read_strings(fid_result, &nvalues, SIGNAL_QUANTITIES);
	ASSERT_TRUE(nvalues == nsignals)

	signal_units = read_strings(fid_result, &nvalues, SIGNAL_UNITS);
	ASSERT_TRUE(nvalues == nsignals)

	signal_comments	= read_strings(fid_result, &nvalues, SIGNAL_COMMENTS);
	ASSERT_TRUE(nvalues == nsignals)

	signals_data = (double *) malloc(nsamples * nsignals * type_size);
	buffer = (double *) malloc(nsamples * type_size);

	dset = H5Dopen (fid_result, SIGNAL_VALUES, H5P_DEFAULT);

	// write the output file
	fid_sdf = H5Fcreate(out_file, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	ASSERT_NO_ERROR(H5LTmake_dataset_double(fid_sdf, TIME, 1, &nsamples, time_data))

	ASSERT_VALID_ID(ds_time = H5Dopen(fid_sdf, TIME, H5P_DEFAULT))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", QUANTITY_ATTR_NAME, "Time"))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", UNIT_ATTR_NAME, "s"))
	ASSERT_NO_ERROR(H5LTset_attribute_string(ds_time, ".", COMMENT_ATTR_NAME, "Time"))

	ASSERT_VALID_ID(space = H5Dget_space (dset))

	// create a dataspace to read the signals
	ASSERT_VALID_ID(memspace = H5Screate_simple(1, &nsamples, NULL))

	for(i = 0; i < nsignals; i++) {
		// select the hyperslab to use for reading
		start[1] = i;
		count[0] = nsamples;
		ASSERT_NO_ERROR(H5Sselect_hyperslab (space, H5S_SELECT_SET, start, NULL, count, NULL))

		// read the data using the previously defined hyperslab
		ASSERT_NO_ERROR(H5Dread (dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, buffer))

		// write the dataset
		ASSERT_NO_ERROR(H5LTmake_dataset_double(fid_sdf, signal_names[i], 1, &nsamples, buffer))

		// set the quantity, unit and comment attributes
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], QUANTITY_ATTR_NAME, signal_quantities[i]))
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], UNIT_ATTR_NAME, signal_units[i]))
		ASSERT_NO_ERROR(H5LTset_attribute_string(fid_sdf, signal_names[i], COMMENT_ATTR_NAME, signal_comments[i]))

		// attach the time as scale
		ASSERT_VALID_ID(ds_signal = H5Dopen(fid_sdf, signal_names[i], H5P_DEFAULT))
		ASSERT_NO_ERROR(H5DSattach_scale(ds_signal, ds_time, 0))
		ASSERT_NO_ERROR(H5Dclose(ds_signal))
	}

out:
	// sdf file
	status = H5Sclose(memspace);
	status = H5Dclose(ds_time);
	status = H5Fclose(fid_sdf);

	// result file
	status = H5Dclose(dset);
	status = H5Fclose(fid_result);

	free(time_data);

	free_string_array(nvalues, signal_names);
	free_string_array(nvalues, signal_quantities);
	free_string_array(nvalues, signal_units);
	free_string_array(nvalues, signal_comments);

    return status;
}
