This repository contains the source code of the Simulation Development Tools

For a description of how to build the native libraries from source please see [VisualStudio/README.md](VisualStudio/README.md)

# Folders

## C

The C sources

## MATLAB

MATLAB utility scripts

## Modelica

The SimDevTools Modelica library

## Python

Python utility scripts

## Simulink

The SimDevTools Modelica library

## ThirdParty

The third party dependencies

## VisualStudio

The VisualStudio solution to build the C sources
